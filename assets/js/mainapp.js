var mainapp = angular.module("mainapp",['ngRoute']);
mainapp.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
    
    .when('/register', {
      templateUrl: '/templates/register.html',
      //controller: 'loginCtrl'
    })
    .when('/forgot', {
      templateUrl: '/templates/forgot.html',
      //controller: 'loginCtrl'
    })
    .otherwise({
      redirectTo: '/',
      caseInsensitiveMatch: true
    })
  }]);
mainapp.controller('loginctrl',['$scope','loginservice',function($scope,loginservice){
	$scope.checklogin = function(){

	    loginservice.login($scope.username,$scope.password).then(function(response) {
	      
	      $scope.formData = {};
	    });
		  
	};
}]);
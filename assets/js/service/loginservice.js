mainapp.service('loginservice', function($http, $q) {
  return {
    'login': function(username,pass) {
      var defer = $q.defer();
      $http.get('/user/login').success(function(resp){
        defer.resolve(resp);
      }).error( function(err) {
        defer.reject(err);
      });
      return defer.promise;
    }
}});
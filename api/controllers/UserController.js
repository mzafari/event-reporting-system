/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	login:function (req,res){
		User.signin({
			username: req.param('username'),
			password: req.param('password')
		}, function (err, user) {
		if (err) return res.negotiate(err);
		if (!user) {
			if (req.isAjax) {
			return res.badRequest('Wrong username or password');
			}
			return res.redirect('/login');
		}

		req.session.user = user.id;
		if (req.isAjax) {
			return res.ok();
		}
		return res.redirect('/');
		});
	},

	logout:function(req,res) {
		req.session.user = null;
		if (req.isAjax) {
			return res.ok('Signed out successfully');
		}
		return res.redirect('/');
	},
	getsignup: function(req,res){
		res.render('signup');
	},
	signup: function(req,res){
		User.signup({
			name: req.param('name'),
			username: req.param('username'),
			password: req.param('password'),
			role: req.param('role'),
			usertype: req.param('usertype')
		}, function (err, user){

			if (err) return res.negotiate(err);
			req.session.user = user.id;
			if (req.isAjax) {
				return res.ok('Signup successful');
			}
			return res.redirect('/welcome');
		});
	}
};


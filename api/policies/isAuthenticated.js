
module.exports = function(req, res, next) {

  if (req.session.user) return next();
  if (req.isAjax) {
    return res.send(401);
  }
  return res.redirect('/login');
};

/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	username:{
  		type:'text',
  		required: true
  	},
  	name:{
  		type:'text',
  		required:true
  	},
  	password:{
  		type:'text',
  		required:true
  	},
  	role:{
  		type:'text',
  		required:true
  	},
  	usertype:{
  		type:'text',
  		required:true
  	}
  },


  signup: function (inputs, cb) {
    User.create({
    	username: inputs.username,
      	name: inputs.name,
      	password: inputs.password,
      	role: inputs.role,
      	usertype: inputs.usertype
    })
    .exec(cb);
  },


  signin: function (inputs, cb) {
    User.findOne({
      	username: inputs.username,
      	password: inputs.password
    })
    .exec(cb);
  }
};

